//
//  ChalkboardResult.swift
//  ChalkboardApi
//
//  Created by Stuart Thomas on 06/02/2021.
//

import Foundation

// MARK: - ChalkboardResult
public struct ChalkboardResult: Codable {
    public let results: [Profile]
    public let info: Info

    public init(results: [Profile], info: Info) {
        self.results = results
        self.info = info
    }
}

// MARK: - Info
public struct Info: Codable {
    public let seed: String
    public let results, page: Int
    public let version: String

    public init(seed: String, results: Int, page: Int, version: String) {
        self.seed = seed
        self.results = results
        self.page = page
        self.version = version
    }
}

// MARK: - Result
public struct Profile: Codable {
    public let name: Name
    public let dob: Dob

    public init(name: Name, dob: Dob) {
        self.name = name
        self.dob = dob
    }
}

// MARK: - Dob
public struct Dob: Codable {
    public let date: String
    public let age: Int

    public init(date: String, age: Int) {
        self.date = date
        self.age = age
    }
}

// MARK: - Name
public struct Name: Codable {
    public let title: Title
    public let first, last: String

    public init(title: Title, first: String, last: String) {
        self.title = title
        self.first = first
        self.last = last
    }
}

public enum Title: String, Codable {
    case Madame
    case Mademoiselle
    case Miss
    case Monsieur
    case Mr
    case Mrs
    case Ms
}
