//
//  HttpService.swift
//  ChalkboardApi
//
//  Created by Stuart Thomas on 06/02/2021.
//

import Foundation

public protocol HttpServiceType {
  typealias Handler = (Data?, URLResponse?, Error?) -> ()
  func request(urlRequest: URLRequest, completion: @escaping Handler)
}

public class HttpService: HttpServiceType {
  
  public init() {}
  
  public func request(urlRequest: URLRequest, completion: @escaping Handler) {
    let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
      completion(data, response, error)
    }
    task.resume()
  }
}
