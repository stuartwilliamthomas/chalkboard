//
//  Request.swift
//  ChalkboardApi
//
//  Created by Stuart Thomas on 06/02/2021.
//

import Foundation

public protocol RequestType {
  typealias Handler = (ChalkboardResult?, Error?) -> ()
  func fetchChalkboardResult(completion: @escaping Handler)
}

public class Request: RequestType {

  let httpService: HttpServiceType
  
  public init(_ httpService: HttpServiceType = HttpService()) {
    self.httpService = httpService
  }

  private var requestURL: URL {
    return URL(string: "https://randomuser.me/api/?results=1000&seed=chalkboard&inc=name,dob")!
  }

  public func fetchChalkboardResult(completion: @escaping Handler) {
    let urlRequest = URLRequest(url: requestURL)
    httpService.request(urlRequest: urlRequest) { [weak self] data, response, error in
      if error == nil {
        guard let data = data else {
          //here I would track that there has been some kind of error as data and error should not both be nil
          completion(nil, error)
          return
        }
        self?.handleChalkboardResultSuccess(completion, data)
      } else {
        // here I would track the error if it wasn't already being tracked API side
        completion(nil, error)
      }
    }
  }

  private func handleChalkboardResultSuccess(_ completion: @escaping RequestType.Handler,
                                             _ data: Data) {
    do {
      let decoder = JSONDecoder()
      decoder.keyDecodingStrategy = .convertFromSnakeCase
      let responseObjects = try decoder.decode(ChalkboardResult.self, from: data)
      completion(responseObjects, nil)
    } catch let parseError {
      // here I would track that there is a parse error
      completion(nil, parseError)
    }
  }

}
