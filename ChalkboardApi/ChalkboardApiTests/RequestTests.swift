//
//  RequestTests.swift
//  ChalkboardApiTests
//
//  Created by Stuart Thomas on 06/02/2021.
//

import XCTest
@testable import ChalkboardApi

class RequestTests: XCTestCase {
  
  var subject: Request!
  
  func testWhenGoodDataIsReturnedTheDataIsParsed() throws {
    subject = Request(MockHttpServiceSuccess())
    let expectation = self.expectation(description: "request called")
    subject.fetchChalkboardResult { result, error in
      expectation.fulfill()
      XCTAssertNil(error)
      XCTAssertNotNil(result)
    }
    waitForExpectations(timeout: 10, handler: nil)
  }
  
  func testWhenBadDataIsReturnedTheDataIsNotParsed() throws {
    subject = Request(MockHttpServiceBadData())
    let expectation = self.expectation(description: "request called")
    subject.fetchChalkboardResult { result, error in
      expectation.fulfill()
      XCTAssertNotNil(error)
      XCTAssertNil(result)
    }
    waitForExpectations(timeout: 10, handler: nil)
  }
  
  func testWhenErrorIsReturnedTheDataIsNotParsed() throws {
    subject = Request(MockHttpServiceFailure())
    let expectation = self.expectation(description: "request called")
    subject.fetchChalkboardResult { result, error in
      expectation.fulfill()
      XCTAssertNotNil(error)
      XCTAssertNil(result)
    }
    waitForExpectations(timeout: 10, handler: nil)
  }
  
}
