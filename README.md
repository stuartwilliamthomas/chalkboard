# Welcome!

The aim is to create a small app that allows you to keep track of birthdays.
When you open the app, you'll be able to see a list of peoples birthdays in order. You will also be able to view a person and it will also tell you how old they are (or will be).

## How to install and run the project.
#### Step1
Go to the /ChalkboardTest directory and open the ChalkboardTest.xcodeproj.

### Step 2

Run the ChalkboardTest target.

## How to run tests

Tests run in the standard manor.

## Reasoning behind your technical choices.

I chose to use the MVVM architectural pattern for a number of reasons. Firstly, it is an architecture I am very familiar with and this would allow me to show you the range of my skills. Secondly, MVVM is a very widely used architecture across industry which means many developers are familiar with it and would be able to work on my code in the future. MVVM also allows for greater decoupling which will make future easier and would reduce potential merge conflicts. MVVMs greater decoupling also allows for easier testing with individual elements able to be tested.

I chose to abstract the data service to a separate framework as, after reading the guide, it seems like you were exploring the option of using graphql for this project and if you were to go into this direction in the future the use of abstraction reduces tight code coupling and would save time.

## Describe any trade-offs you needed to make and your reasoning.
Noe that occur to me.

## Point out anything you might do differently if you had more time.
If I were to have more time I would have increased the number of tests, specifically I would have introduced UI tests. I would have also increased to efficiency of the profile image setting code which runs twice for each user, once for the list and once for the profile.

## Your contact details and any public profiles on developer networks like GitHub, BitBucket, Stack Overflow, etc.
Email - stuartwilliamthomas@gmail.com
Phone - 07794950020
Further iOS projects can be found on this Gitlab profile.
