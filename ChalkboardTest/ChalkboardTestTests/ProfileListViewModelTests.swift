//
//  ProfileListViewModelTests.swift
//  ChalkboardTestTests
//
//  Created by Stuart Thomas on 07/02/2021.
//

import XCTest
@testable import ChalkboardTest
@testable import ChalkboardApi

class ProfileListViewModelTests: XCTestCase {
  
  var subject: ProfileListViewModel!
  
  func testResultsAreReturnedWithASuccessfulRequest() throws {
    subject = ProfileListViewModel(request: MockRequestSuccess())
    subject.fetchChalkboardResult { error in
      XCTAssertNil(error)
      XCTAssertNotNil(self.subject.result)
    }
  }
  
  func testErrorIsReturnedWithAFailedRequest() throws {
    subject = ProfileListViewModel(request: MockRequestFailure())
    subject.fetchChalkboardResult { error in
      XCTAssertNotNil(error)
      XCTAssertNil(self.subject.result)
    }
  }
  
}
