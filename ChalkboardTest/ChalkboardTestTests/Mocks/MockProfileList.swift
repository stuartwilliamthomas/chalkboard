//
//  MockProfileList.swift
//  ChalkboardTestTests
//
//  Created by Stuart Thomas on 07/02/2021.
//

import XCTest
@testable import ChalkboardTest
@testable import ChalkboardApi

class MockProfileList {
  
  static var profile: Profile {
    let name = Name(title: .Mr, first: "John", last: "Smith")
    let dob = Dob(date: "01-01-1960", age: 60)
    
    return Profile(name: name, dob: dob)
  }
  
  static var info: Info {
    return Info(seed: "", results: 1, page: 1, version: "")
  }
  
}
