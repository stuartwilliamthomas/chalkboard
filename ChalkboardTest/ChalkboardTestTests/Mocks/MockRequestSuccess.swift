//
//  MockRequestSuccess.swift
//  ChalkboardTestTests
//
//  Created by Stuart Thomas on 07/02/2021.
//

import XCTest
@testable import ChalkboardTest
@testable import ChalkboardApi

class MockRequestSuccess: RequestType {
  func fetchChalkboardResult(completion: @escaping Handler) {
    let results = ChalkboardResult(results: [MockProfileList.profile], info: MockProfileList.info)
    completion(results, nil)
  }
}
