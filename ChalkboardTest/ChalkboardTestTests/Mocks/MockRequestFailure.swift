//
//  MockRequestFailure.swift
//  ChalkboardTestTests
//
//  Created by Stuart Thomas on 07/02/2021.
//

import XCTest
@testable import ChalkboardTest
@testable import ChalkboardApi

class MockRequestFailure: RequestType {
  
  func fetchChalkboardResult(completion: @escaping Handler) {
    completion(nil, MockError())
  }
  
}

class MockError: Error {
  var localizedDescription: String = "Test Error"
}
