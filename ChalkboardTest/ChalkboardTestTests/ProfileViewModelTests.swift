//
//  ProfileViewModelTests.swift
//  ChalkboardTestTests
//
//  Created by Stuart Thomas on 07/02/2021.
//

import XCTest
@testable import ChalkboardTest
@testable import ChalkboardApi

class ProfileViewModelTests: XCTestCase {
  
  var subject: ProfileViewModel!
  
  override func setUp() {
    subject = ProfileViewModel(profile: MockProfileList.profile)
  }
  
  func testCorrectValuesAreReturned() throws {
    XCTAssertEqual(subject.ageText, "60 YEARS OLD")
    XCTAssertEqual(subject.firstNameText, "John")
    
  }
  
}
