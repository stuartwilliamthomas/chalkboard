//
//  ProfileListViewController.swift
//  ChalkboardTest
//
//  Created by Stuart Thomas on 06/02/2021.
//

import UIKit

class ProfileListViewController: UIViewController {
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  var dataSource: ProfileListDataSource?
  var viewModel: ProfileListViewModel!
  
  override func viewDidLoad() {
    viewModel = ProfileListViewModel()
    dataSource = ProfileListDataSource(collectionView, viewModel, ProfileCoordinator(controller: self))
    super.viewDidLoad()
    fetchResults()
  }
  
  private func fetchResults() {
    viewModel?.fetchChalkboardResult(completion: { [weak self] error in
      if error != nil {
        self?.showRequestError()
      } else {
        DispatchQueue.main.async {
          //self?.title = self?.viewModel.title
          self?.collectionView.reloadData()
        }
      }
    })
  }
  
  private func showRequestError() {
    let alert = UIAlertController(title: "Something went wrong with your request",
                                  message: "Do you want to try again?",
                                  preferredStyle: UIAlertController.Style.alert)
    
    alert.addAction(UIAlertAction(title: "Try Again?",
                                  style: .default,
                                  handler: { [weak self] _ in
      self?.fetchResults()
    }))
    alert.addAction(UIAlertAction(title: "Cancel",
                                  style: .cancel))
    DispatchQueue.main.async {
      self.present(alert, animated: true, completion: nil)
    }
  }


}
