//
//  ProfileListCollectionViewCell.swift
//  ChalkboardTest
//
//  Created by Stuart Thomas on 06/02/2021.
//

import Foundation
import UIKit
import ChalkboardApi

class ProfileListCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var profileImage: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var birthdayLabel: UILabel!
  
  var profile: Profile? {
    didSet {
      setupUI()
    }
  }
  
  private func setupUI() {
    let initialsImageGenerator = InitialsImageGenerator()
    profileImage.image =  initialsImageGenerator.generate(firstName: profile?.name.first,
                                                          lastName: profile?.name.last)
    nameLabel.text = buildFullName()
    birthdayLabel.text = formatDateFromDateString(profile?.dob.date ?? "")
  }
  
  private func buildFullName() -> String {
    guard let name = profile?.name else { return "" }
    return "\(name.title) \(name.first) \(name.last)"
  }
  
  func formatDateFromDateString(_ dateString: String) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "YYYY-MM-DD'T'HH:mm:ss.SSS'Z'"
    let date = formatter.date(from: dateString)!
    formatter.dateFormat = "dd-MM-yyyy"
    return formatter.string(from: date)
  }
  
}
