//
//  ProfileDataSource.swift
//  ChalkboardTest
//
//  Created by Stuart Thomas on 06/02/2021.
//

import Foundation
import UIKit

class ProfileListDataSource: NSObject,
                             UICollectionViewDataSource,
                             UICollectionViewDelegate,
                             UICollectionViewDelegateFlowLayout {
  
  let collectionView: UICollectionView
  let profileCoordinator: ProfileCoordinator
  var viewModel: ProfileListViewModel {
    didSet {
      collectionView.reloadData()
    }
  }
  
  init(_ collectionView: UICollectionView,
       _ viewModel: ProfileListViewModel,
       _ profileCoordinator: ProfileCoordinator) {
    self.collectionView = collectionView
    self.viewModel = viewModel
    self.profileCoordinator = profileCoordinator
    super.init()
    collectionView.dataSource = self
    collectionView.delegate = self
    registerCell()
  }
  
  private func registerCell() {
    let cellDescription = String(describing: ProfileListCollectionViewCell.self)
    collectionView.register(UINib(nibName: cellDescription, bundle: nil),
                            forCellWithReuseIdentifier: cellDescription)
  }
  
  //MARK: UICollectionViewDataSource
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return viewModel.profileList?.count ?? 0
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let cellIdentifier = String(describing: ProfileListCollectionViewCell.self)
      guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,
                                                                  for: indexPath) as? ProfileListCollectionViewCell else {
        //track error here
        print(">>>>> error creating cell")
        return UICollectionViewCell()
      }
      cell.profile = viewModel.profileList?[indexPath.row]
      return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard let profile = viewModel.profileList?[indexPath.row] else { return }
    profileCoordinator.openProfile(for: profile)
  }
  
  //MARK: UICollectionViewDelegateFlowLayout
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    let cellHeight = 100
    let cellWidth = collectionView.frame.width - 16
    return CGSize(width: cellWidth, height: CGFloat(cellHeight))
  }
  
}
