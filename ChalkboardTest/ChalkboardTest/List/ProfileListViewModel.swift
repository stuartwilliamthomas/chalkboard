//
//  ProfileViewModel.swift
//  ChalkboardTest
//
//  Created by Stuart Thomas on 06/02/2021.
//

import Foundation
import ChalkboardApi

class ProfileListViewModel {
  
  typealias Handler = (Error?) -> ()
  
  var request: RequestType?
  var result: ChalkboardResult?
  var profileList: [Profile]?
  
  init(request: RequestType = Request()) {
    self.request = request
  }
  
  func fetchChalkboardResult(completion: @escaping Handler) {
    request?.fetchChalkboardResult { [weak self] result, error in
      self?.result = result
      self?.profileList = result?.results
      completion(error)
    }
    
  }
  
}
