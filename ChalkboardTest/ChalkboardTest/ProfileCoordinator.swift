//
//  ProfileCoordinator.swift
//  ChalkboardTest
//
//  Created by Stuart Thomas on 06/02/2021.
//

import Foundation
import UIKit
import ChalkboardApi

class ProfileCoordinator {
  
  weak var controller: UIViewController?
  
  init(controller: UIViewController) {
    self.controller = controller
  }
  
  func openProfile(for profile: Profile) {
    let profileStoryboard = UIStoryboard(name: "ProfileViewController", bundle: .main)
    guard let profileViewController: ProfileViewController = profileStoryboard.instantiateInitialViewController() as? ProfileViewController else { return }
    profileViewController.viewModel = ProfileViewModel(profile: profile)
    controller?.navigationController?.pushViewController(profileViewController,
                                                         animated: true)
  }
  
}
