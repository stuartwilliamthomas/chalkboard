//
//  InitialsImageGenerator.swift
//  ChalkboardTest
//
//  Created by Stuart Thomas on 07/02/2021.
//

import Foundation
import UIKit

class InitialsImageGenerator {
  
  func generate(firstName: String?, lastName: String?) -> UIImage? {
    let frame = CGRect(x: 0, y: 0, width: 80, height: 80)
    let nameLabel = UILabel(frame: frame)
    nameLabel.layer.cornerRadius = nameLabel.frame.size.width/2
    nameLabel.layer.masksToBounds = true
    nameLabel.textAlignment = .center
    nameLabel.backgroundColor = .random()
    nameLabel.textColor = .white
    nameLabel.font = UIFont.boldSystemFont(ofSize: 40)
    guard let firstName = firstName,
          let lastName = lastName else { return nil }
    nameLabel.text = "\(firstName.prefix(1))\(lastName.prefix(1))"
    UIGraphicsBeginImageContext(frame.size)
    if let currentContext = UIGraphicsGetCurrentContext() {
      nameLabel.layer.render(in: currentContext)
      let nameImage = UIGraphicsGetImageFromCurrentImageContext()
      return nameImage
    }
    return nil
  }
  
}

extension CGFloat {
  static func random() -> CGFloat {
    return CGFloat(arc4random()) / CGFloat(UInt32.max)
  }
}

extension UIColor {
  static func random() -> UIColor {
    return UIColor(
      red:   .random(),
      green: .random(),
      blue:  .random(),
      alpha: 1.0
    )
  }
}
