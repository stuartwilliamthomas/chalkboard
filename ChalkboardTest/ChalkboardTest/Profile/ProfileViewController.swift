//
//  ProfileViewController.swift
//  ChalkboardTest
//
//  Created by Stuart Thomas on 07/02/2021.
//

import UIKit

class ProfileViewController: UIViewController {
  
  var viewModel: ProfileViewModel?
  
  @IBOutlet weak var profileImageView: UIImageView!
  @IBOutlet weak var firstNameLabel: UILabel!
  @IBOutlet weak var ageLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.title = "Profile"
    setupUI()
  }
  
  private func setupUI() {
    guard let viewModel = viewModel else { return }
    profileImageView.image = viewModel.profilePhoto
    firstNameLabel.text = viewModel.firstNameText
    ageLabel.text = viewModel.ageText
  }
  
  @IBAction func didPressGoBackButton(_ sender: UIButton) {
    _ = navigationController?.popViewController(animated: true)
  }
  
    
}
