//
//  ProfileViewModel.swift
//  ChalkboardTest
//
//  Created by Stuart Thomas on 07/02/2021.
//

import Foundation
import UIKit
import ChalkboardApi

class ProfileViewModel {
  
  let profile: Profile
  
  var firstNameText: String {
    return profile.name.first
  }
  
  var ageText: String {
    return "\(profile.dob.age) YEARS OLD"
  }
  
  var profilePhoto: UIImage {
    let initialsImageGenerator = InitialsImageGenerator()
    return initialsImageGenerator.generate(firstName: profile.name.first,
                                           lastName: profile.name.last)!
  }
  
  init(profile: Profile) {
    self.profile = profile
  }
  
}
